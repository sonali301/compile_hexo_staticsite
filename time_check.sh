#!/bin/bash
m=`date -u +%M`
echo "You are running this build at $m th minute of this hour"
# if m greater than 30, then exit and give error
if [ $m -lt 30 ]; then
echo "Build should sucessfully run at this time, please scroll below and check logs for any issues"
fi
if [ $m -gt 30 ]; then
echo "Build is not allowed in 2nd half of this hour, kindly rerun the build in first half of any hour for successful execution "
exit 1
fi
 

